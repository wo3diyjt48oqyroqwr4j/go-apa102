package apa102

import (
	"fmt"
	"github.com/stianeikeland/go-rpio"
	"log"
	"time"
)

const (
	startFrame = "00000000000000000000000000000000"
	dataRate   = 100 // nanoSecond delay ; lower = faster
)

var (
	cp, dp rpio.Pin
)

type New struct {
	DataPin int
	ClkPin  int
	Multi   struct {
		r  []int
		g  []int
		b  []int
		br []int
	}
}

func (n *New) Start() {
	if err := rpio.Open(); err != nil {
		log.Println(err)
	}
	cp, dp = rpio.Pin(n.ClkPin), rpio.Pin(n.DataPin)
	cp.Output()
	dp.Output()
}

func (n *New) Close() {
	time.Sleep(time.Millisecond * time.Duration(500))
	if err := rpio.Close(); err != nil {
		log.Println(err)
	}
}

func intToBinary(input int) string {
	bits := fmt.Sprintf("%b", input)
	for {
		if len(bits) == 8 {
			break
		}
		bits = "0" + bits
	}
	return bits
}

func (n *New) bangBits(input string) { // input: binary string = "00000000"
	for x := 0; x < len(input); x++ {
		if input[x:x+1] == "0" {
			dp.Low()
		}
		if input[x:x+1] == "1" {
			dp.High()
		}
		cp.High()
		time.Sleep(time.Nanosecond * time.Duration(dataRate))
		cp.Low()
		time.Sleep(time.Nanosecond * time.Duration(dataRate))
	}
	dp.Low()
}

func brightnessCal(input int) string {
	if input > 31 {
		input = 31
	}
	return "111" + intToBinary(input)[3:8]
}

// PixelWrite : (red, green ,blue) bit bang spi to led ; values : 0 - 255
func (n *New) PixelWrite(red, green, blue, brightness int) {
	cp.Low()
	dp.Low()
	n.bangBits(startFrame)
	n.bangBits(brightnessCal(brightness))
	n.bangBits(intToBinary(blue))
	n.bangBits(intToBinary(green))
	n.bangBits(intToBinary(red))
	n.bangBits("11111111111111111111111111111111")
	time.Sleep(time.Nanosecond * time.Duration(15))
}

// AppendMulti : red green blue brightness
func (n *New) AppendMulti(r, g, b, br int) {
	n.Multi.r = append(n.Multi.r, r)
	n.Multi.g = append(n.Multi.g, g)
	n.Multi.b = append(n.Multi.b, b)
	n.Multi.br = append(n.Multi.br, br)
}

func (n *New) ClearMulti() {
	n.Multi.r = []int{}
	n.Multi.g = []int{}
	n.Multi.b = []int{}
	n.Multi.br = []int{}
}

// WriteMulti : write data appended with AppendMulti
func (n *New) WriteMulti() {
	endFrame := "11111111111111111111111111111111"
	ledsToWrite := len(n.Multi.br)
	half := ledsToWrite / 2
	if half > 32 {
		for {
			endFrame = endFrame + "1"
			if len(endFrame) == half {
				break
			}
		}
	}
	cp.Low()
	dp.Low()
	n.bangBits(startFrame)
	for x := 0; x < ledsToWrite; x++ {
		n.bangBits(brightnessCal(n.Multi.br[x]))
		n.bangBits(intToBinary(n.Multi.b[x]))
		n.bangBits(intToBinary(n.Multi.g[x]))
		n.bangBits(intToBinary(n.Multi.r[x]))
	}
	n.bangBits(endFrame)
}
